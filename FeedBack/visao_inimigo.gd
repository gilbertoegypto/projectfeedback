extends Area2D

var item = false

func _ready():
	#self.set_collision_mask(8)
	#self.set_collision_layer(8)
	#print(self.collision_layer)
	#print("layer")
	#print(self.collision_mask)
	#print("mask")
	pass
	
func _physics_process(delta):
	if Input.is_action_just_pressed("INTERACT"):
		self.set_collision_mask(16)
		self.set_collision_layer(16)
		yield(get_tree().create_timer(5.0),"timeout")
		self.set_collision_mask(8)
		self.set_collision_layer(8)
	if Input.is_action_just_pressed("ITEM"):
		self.set_collision_mask(16)
		self.set_collision_layer(16)
		yield(get_tree().create_timer(5.0),"timeout")
		self.set_collision_mask(8)
		self.set_collision_layer(8)
	pass


