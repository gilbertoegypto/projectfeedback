extends KinematicBody2D

export var walk_speed = 210
export var movement = Vector2()
export var push_speed = 150
var jogador = null

func _physics_process(delta):
	
	var collision = move_and_collide(movement*delta)
	movement = Vector2()
	movement=movement.normalized()
	move_and_slide(movement*walk_speed)
	if collision: 
		if collision.collider.is_in_group("player"):
			jogador = collision.collider
			movement = jogador.movement
	else:
		jogador = null


func _on_Player_caixa_mov(valor):
	
	movement= valor
	
	movement=movement.normalized()
	move_and_slide(movement*walk_speed)
