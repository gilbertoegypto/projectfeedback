extends Node2D

var unlock = 0
signal unlockKey2(i)

func _ready():
	$PhysicsBox/ReflectionAzul.visible = false
	$PhysicsBox/ReflectionCinza.visible = true
	$PhysicsBox/ReflectionVerde.visible = false
	$PhysicsBox/ReflectionVermelho.visible = false
	
	$PhysicsBox2/ReflectionAzul.visible = false
	$PhysicsBox2/ReflectionCinza.visible = false
	$PhysicsBox2/ReflectionVerde.visible = true
	$PhysicsBox2/ReflectionVermelho.visible = false
	
	$PhysicsBox3/ReflectionAzul.visible = true
	$PhysicsBox3/ReflectionCinza.visible = false
	$PhysicsBox3/ReflectionVerde.visible = false
	$PhysicsBox3/ReflectionVermelho.visible = false
	
	$PhysicsBox4/ReflectionAzul.visible = false
	$PhysicsBox4/ReflectionCinza.visible = false
	$PhysicsBox4/ReflectionVerde.visible = false
	$PhysicsBox4/ReflectionVermelho.visible = true

func _physics_process(delta):
	if unlock >=4:
		emit_signal("unlockKey2",1)
		unlock = 0



func _on_Area_de_caixa5_pressed5():
	unlock+=1
	$Area_de_caixa5/CollisionShape2D.queue_free()


func _on_Area_de_caixa4_pressed4():
	unlock+=1
	$Area_de_caixa4/CollisionShape2D.queue_free()

func _on_Area_de_caixa3_pressed3():
	unlock+=1
	$Area_de_caixa3/CollisionShape2D.queue_free()


func _on_Area_de_caixa2_pressed2():
	unlock+=1
	$Area_de_caixa2/CollisionShape2D.queue_free()


func _on_Area_de_caixa1_pressed1():
	unlock+=1
	$Area_de_caixa1/CollisionShape2D.queue_free()
