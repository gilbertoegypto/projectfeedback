extends Node2D

var unlock = 0
signal unlockKey2(i)

func _physics_process(delta):
	if unlock >=4:
		emit_signal("unlockKey2",1)
		unlock = 0



func _on_Area_de_caixa5_pressed5():
	unlock+=1
	$Area_de_caixa5/CollisionShape2D.queue_free()


func _on_Area_de_caixa4_pressed4():
	unlock+=1
	$Area_de_caixa4/CollisionShape2D.queue_free()

func _on_Area_de_caixa3_pressed3():
	unlock+=1
	$Area_de_caixa3/CollisionShape2D.queue_free()


func _on_Area_de_caixa2_pressed2():
	unlock+=1
	$Area_de_caixa2/CollisionShape2D.queue_free()


func _on_Area_de_caixa1_pressed1():
	unlock+=1
	$Area_de_caixa1/CollisionShape2D.queue_free()
