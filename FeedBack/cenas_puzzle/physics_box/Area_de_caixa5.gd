extends Area2D

onready var animated_sprite : AnimatedSprite = $AnimatedSprite
signal pressed5
signal unpressed5


func _ready():
	connect("body_entered", self, "_on_body_entered")
	connect("body_exited", self, "_on_body_exited")
	

func _on_body_entered(body: PhysicsBody2D) -> void:
	if not body is PhysicsBox5:
		return
	
	emit_signal("pressed5")
	print("pressao5")
		
func _on_body_exited(body: PhysicsBody2D) -> void:
	if not body is PhysicsBox5:
		return
	
	emit_signal("unpressed5")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
