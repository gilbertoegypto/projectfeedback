extends Area2D

onready var animated_sprite : AnimatedSprite = $AnimatedSprite
signal pressed1
signal unpressed1


func _ready():
	connect("body_entered", self, "_on_body_entered")
	connect("body_exited", self, "_on_body_exited")
	

func _on_body_entered(body: PhysicsBody2D) -> void:
	if not body is PhysicsBox1:
		return
	
	emit_signal("pressed1")
	print("pressao1")
		
func _on_body_exited(body: PhysicsBody2D) -> void:
	if not body is PhysicsBox1:
		return
	
	emit_signal("unpressed1")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
