extends Area2D

onready var animated_sprite : AnimatedSprite = $AnimatedSprite
signal pressed4
signal unpressed4


func _ready():
	connect("body_entered", self, "_on_body_entered")
	connect("body_exited", self, "_on_body_exited")
	

func _on_body_entered(body: PhysicsBody2D) -> void:
	if not body is PhysicsBox4:
		return
	
	emit_signal("pressed4")
	print("pressao4")
		
func _on_body_exited(body: PhysicsBody2D) -> void:
	if not body is PhysicsBox4:
		return
	
	emit_signal("unpressed4")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
