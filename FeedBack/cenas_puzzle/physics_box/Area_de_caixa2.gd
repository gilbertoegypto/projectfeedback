extends Area2D

onready var animated_sprite : AnimatedSprite = $AnimatedSprite
signal pressed2
signal unpressed2


func _ready():
	connect("body_entered", self, "_on_body_entered")
	connect("body_exited", self, "_on_body_exited")
	

func _on_body_entered(body: PhysicsBody2D) -> void:
	if not body is PhysicsBox2:
		return
	
	emit_signal("pressed2")
	print("pressao2")
		
func _on_body_exited(body: PhysicsBody2D) -> void:
	if not body is PhysicsBox2:
		return
	
	emit_signal("unpressed2")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
