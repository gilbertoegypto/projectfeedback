extends Box
class_name PhysicsBox2
var velo = 100
#var velocity = 125

func push(velocity : Vector2) -> void:
	move_and_slide(velocity, Vector2())
	
func cinza():
	$ReflectionAzul.visible = false
	$ReflectionCinza.visible = true
	$ReflectionVerde.visible = false
	$ReflectionVermelho.visible = false
func vermelho():
	$ReflectionAzul.visible = false
	$ReflectionCinza.visible = false
	$ReflectionVerde.visible = false
	$ReflectionVermelho.visible = true
func azul():
	$ReflectionAzul.visible = true
	$ReflectionCinza.visible = false
	$ReflectionVerde.visible = false
	$ReflectionVermelho.visible = false
func verde():
	$ReflectionAzul.visible = false
	$ReflectionCinza.visible = false
	$ReflectionVerde.visible = true
	$ReflectionVermelho.visible = false

