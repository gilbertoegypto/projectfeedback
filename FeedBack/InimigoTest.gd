extends KinematicBody2D
var player = null;
var move = Vector2.ZERO
var speed = 1
export var move_speed = 150
var holograma = false



func _physics_process(delta):
	move = Vector2.ZERO
	
	if player != null:
		move = position.direction_to(player.position) * speed
	else:
		move = Vector2.ZERO

	_changing_sprites()
	move = move.normalized()
	move_and_slide(move*move_speed)
	
	

func _on_Area2D_body_entered(body):
	if body != self:
		player = body


func _on_Area2D_body_exited(body):
	player = null
	
func _changing_sprites():
	if move.x < 0:
		$Sprite/EnemyAnimation/EnemyAnimationSprite.flip_h=true
	if move.x > 0:
		$Sprite/EnemyAnimation/EnemyAnimationSprite.flip_h=false
