extends KinematicBody2D

export var walk_speed = 210
export var movement = Vector2()
export var facingDir = Vector2()
export var interactDist = 70
export var flashLight = true
export var unlock = false
export var hologram = 20
export var player_life = 20
signal interaction(command)
signal death(i)
export var flare = false
signal flare(location, facingdir)
signal activateLight(yorn)
signal hologramCollected(i)
var idle = true
var attack = false
signal attack(atk)
var attacking = false
var damage = false
var waking = true
var frus = false
var res_path

var free_to_go = false
var got_light = false

func _ready():
	res_path = get_tree().current_scene.filename
	print(res_path)
	$Light2D_outer.enabled = not $Light2D_outer.enabled
	$Light2d_inner.enabled = not $Light2d_inner.enabled
	facingDir = Vector2(1,0)
	if facingDir.x < 0:
		$AnimatedSprite.set_flip_h( true )
		$Reflection.set_flip_h(true)
	if facingDir.x > 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	_Waking()
	
func _Waking():
	#pass
	frus = false
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	$AnimatedSprite/AnimationPlayer.play("Waking")
	$Reflection/AnimationPlayer.play("Waking")
	yield(get_tree().create_timer(10.0),"timeout")
	$AnimatedSprite/AnimationPlayer.play("Idle_Left")
	$Reflection/AnimationPlayer.play("Idle_Left")
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("First_Dialogue")
	yield(get_tree().create_timer(2.5),"timeout")
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.stop()
	waking = false
	frus = true

func _physics_process(delta):
	var collision = move_and_collide(movement*delta)
	if collision:
		if collision.collider.is_in_group("inimigo_tutorial"):
			pass
		
	movement = Vector2()
	#Input
	if Input.is_action_pressed("DOWN") and  attack == false and damage == false and waking == false:
		movement.y+=1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		idle = false
	if Input.is_action_just_released("DOWN") and  attack == false and damage == false and waking == false:
		idle = true
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		
	if Input.is_action_pressed("UP") and  attack == false and damage == false and waking == false:
		movement.y+=-1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		idle = false
		
	if Input.is_action_just_released("UP") and  attack == false and damage == false and waking == false:
		idle = true
		movement.y+=-1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		
	if Input.is_action_pressed("LEFT") and  attack == false and damage == false and waking == false:
		movement.x+=-1
		facingDir = Vector2(-1,0)
		idle = false
		
	if Input.is_action_just_released("LEFT") and  attack == false and damage == false and waking == false:
		idle = true
		facingDir = Vector2(-1,0)
			
	if Input.is_action_pressed("RIGHT") and  attack == false and damage == false and waking == false:
		movement.x+=1
		facingDir = Vector2(1,0)
		idle = false
	if Input.is_action_just_released("RIGHT") and  attack == false and damage == false and waking == false:
		idle = true
		facingDir = Vector2(1,0)
			
	if Input.is_action_just_pressed("INTERACT") and  attack == false and damage == false and waking == false:
		attack = true
		#print(attack)
		idle = false
		#_on_attacking()
		yield(get_tree().create_timer(2.0),"timeout")
		idle = true
		attack = false
		#print(attack)
	if Input.is_action_just_pressed("ITEM")and frus == true and  attack == false and damage == false and waking == false:
		_Frustrated()
		
		
	
	_changing_sprites()
	movement=movement.normalized()
	#move the player
	move_and_slide(movement*walk_speed)
	
	
func _changing_sprites():
	if facingDir.y!=0 and idle == false and  attack == false and damage == false and waking == false:
		$AnimatedSprite/AnimationPlayer.play("Walking_Left")
		$Reflection/AnimationPlayer.play("Walking_Left")
		#$AnimatedSprite.set_flip_h( true )

	if facingDir.x!=0and idle == false and  attack == false and damage == false and waking == false:
		$AnimatedSprite/AnimationPlayer.play("Walking_Left")
		$Reflection/AnimationPlayer.play("Walking_Left")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
			$Reflection.set_flip_h(false)
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
			$Reflection.set_flip_h(true)
			
	if idle == true and  attack == false and damage == false and waking == false:
		$AnimatedSprite/AnimationPlayer.play("Idle_Left")
		$Reflection/AnimationPlayer.play("Idle_Left")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( true )
			$Reflection.set_flip_h(true)
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( false )
			$Reflection.set_flip_h(false)
			
	if idle == false and attack == true and damage == false:
		$AnimatedSprite/AnimationPlayer.play("Attacking")
		$Reflection/AnimationPlayer.play("Attacking")
		#$AnimatedSprite.play("Attacking")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
			$Reflection.set_flip_h(false)
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
			$Reflection.set_flip_h(true)
		
		



func take_damage():
	damage = true
	if player_life > 0 :
		_damage()
		#yield(get_tree().create_timer(3.0),"timeout")
	elif player_life <= 0 :
		_damage()
		#pass
		#yield(get_tree().create_timer(3.0),"timeout")
		#emit_signal("death",1)
		self.visible = false
		yield(get_tree().create_timer(3.0),"timeout")
		get_tree().reload_current_scene()
		_restart()

	
func _restart():
	$Light2D_outer.enabled = not $Light2D_outer.enabled
	$Light2d_inner.enabled = not $Light2d_inner.enabled
	facingDir = Vector2(1,0)
	_Waking()
	
func _damage():
	_Pissed()
	player_life-=5
	$AnimatedSprite/AnimationPlayer.play("Damaged")
	$Reflection/AnimationPlayer.play("Damaged")
	if facingDir.x < 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	if facingDir.x > 0:
		$AnimatedSprite.set_flip_h( true )
		$Reflection.set_flip_h(true)
	yield(get_tree().create_timer(0.7),"timeout")
	$AnimatedSprite/AnimationPlayer.play("Idle_Left")
	$Reflection/AnimationPlayer.play("Idle_Left")
	if facingDir.x < 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	if facingDir.x > 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	damage = false
	

func _on_HologramItem1_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_HologramItem2_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_Area2D_body_entered(body):
	$Light2D_outer.enabled =  not $Light2D_outer.enabled
	$Light2d_inner.enabled =  not $Light2d_inner.enabled
	emit_signal("activateLight", true)
	got_light = true


func _on_attackArea_body_entered(body):
	if body.is_in_group("inimigo_tutorial"):
		body.take_damage()
		attacking = true
		
		
#func _on_attacking():
#	if attacking == true:
#		emit_signal("attack",1)
		



func _on_attackArea_area_entered(area):
	if area.is_in_group("inimigo_tutorial"):
		area.take_damage()
		attacking = true
		print("attacking=",attacking)

func _restore_full_health():
	free_to_go = true
	if player_life < 20:
		player_life = 20
		print("Sua vida foi restaurada completamente!")
	else:
		print("Você coletou item, porém sua vida já está cheia!")


func _on_Event_Waking_Enemy_body_entered(body):
	if body.is_in_group("Player"):
		_Ballon_Popping()
	
func _Ballon_Popping():
	frus = false
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Poping")
	yield(get_tree().create_timer(0.8),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Idle")
	yield(get_tree().create_timer(0.8),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Fading")
	yield(get_tree().create_timer(0.9),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	frus = true

func _Frustrated():
	frus = false
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Poping")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Frustrated")
	yield(get_tree().create_timer(1.6),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Fading")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	frus = true
	
func _Pissed():
	frus = false
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Poping")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Pissed")
	yield(get_tree().create_timer(1.6),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Fading")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	frus = true


func _on_Entrance_body_entered(body):
	if body.is_in_group("Player"):
		if got_light == true:
			if free_to_go == true:
				yield(get_tree().create_timer(0.3),"timeout")
				get_tree().change_scene("res://CenaJogoMain.tscn")
			else:
				$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Not_free_to_go")
				yield(get_tree().create_timer(2.5),"timeout")
				$Dialogue_Box/AnimatedSprite/AnimationPlayer.stop()
		else:
			$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("First_Dialogue")
			yield(get_tree().create_timer(2.5),"timeout")
			$Dialogue_Box/AnimatedSprite/AnimationPlayer.stop()
