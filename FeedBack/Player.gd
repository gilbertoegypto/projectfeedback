extends KinematicBody2D

export var walk_speed = 210
export var movement = Vector2()
export var facingDir = Vector2()
export var interactDist = 70
export var flashLight = true
export var unlock = false
export var hologram = 20
export var player_life = 20
signal interaction(command)
signal death(i)
export var flare = false
signal flare(location, facingdir)
signal activateLight(yorn)
signal hologramCollected(i)
var idle = true
var attack = false
signal attack(atk)
var attacking = false
var damage = false

var puzzle_1 = false

func _ready():
	facingDir = Vector2(1,0)

func _physics_process(delta):
	_puzzle_1()
	
	
	var collision = move_and_collide(movement*delta)
	if collision:
		if collision.collider.is_in_group("inimigo_dia"):
			pass
		
	movement = Vector2()
	#Input
	if Input.is_action_pressed("DOWN") and  attack == false and damage == false:
		movement.y+=1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		idle = false
	if Input.is_action_just_released("DOWN") and  attack == false and damage == false:
		idle = true
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		
	if Input.is_action_pressed("UP") and  attack == false and damage == false:
		movement.y+=-1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		idle = false
		
	if Input.is_action_just_released("UP") and  attack == false and damage == false:
		idle = true
		movement.y+=-1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		
	if Input.is_action_pressed("LEFT") and  attack == false and damage == false:
		movement.x+=-1
		facingDir = Vector2(-1,0)
		idle = false
		
	if Input.is_action_just_released("LEFT") and  attack == false and damage == false:
		idle = true
		facingDir = Vector2(-1,0)
			
	if Input.is_action_pressed("RIGHT") and  attack == false and damage == false:
		movement.x+=1
		facingDir = Vector2(1,0)
		idle = false
	if Input.is_action_just_released("RIGHT") and  attack == false and damage == false:
		idle = true
		facingDir = Vector2(1,0)
			
	if Input.is_action_just_pressed("INTERACT") and  attack == false and damage == false:
		attack = true
		print(attack)
		idle = false
		#_on_attacking()
		yield(get_tree().create_timer(2.0),"timeout")
		idle = true
		attack = false
		print(attack)
		
		
		
	
	_changing_sprites()
	movement=movement.normalized()
	#move the player
	move_and_slide(movement*walk_speed)
	
	
func _changing_sprites():
	if facingDir.y!=0 and idle == false and  attack == false and damage == false:
		$AnimatedSprite/AnimationPlayer.play("Walking_Left")
		#$AnimatedSprite.set_flip_h( true )

	if facingDir.x!=0and idle == false and  attack == false and damage == false:
		$AnimatedSprite/AnimationPlayer.play("Walking_Left")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
			
	if idle == true and  attack == false and damage == false:
		$AnimatedSprite/AnimationPlayer.play("Idle_Left")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( true )
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( false )
			
	if idle == false and attack == true and damage == false:
		$AnimatedSprite/AnimationPlayer.play("Attacking")
		#$AnimatedSprite.play("Attacking")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
		
		



func take_damage():
	damage = true
	if player_life > 0 :
		_damage()
		#yield(get_tree().create_timer(3.0),"timeout")
	elif player_life <= 0 :
		_damage()
		#pass
		#yield(get_tree().create_timer(3.0),"timeout")
		#emit_signal("death",1)
		self.visible = false
		yield(get_tree().create_timer(3.0),"timeout")
		get_tree().reload_current_scene()
	

func _damage():
	player_life-=5
	$AnimatedSprite/AnimationPlayer.play("Damaged")
	yield(get_tree().create_timer(0.7),"timeout")
	$AnimatedSprite/AnimationPlayer.play("Idle_Left")
	#$AnimatedSprite/AnimationPlayer.play("Damaged")
	if facingDir.x < 0:
		$AnimatedSprite.set_flip_h( false )
	if facingDir.x > 0:
		$AnimatedSprite.set_flip_h( false )
	damage = false
	

func _on_HologramItem1_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_HologramItem2_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_Area2D_body_entered(body):
	flare=true
	emit_signal("activateLight", true)


func _on_attackArea_body_entered(body):
	if body.is_in_group("inimigo_dia"):
		body.take_damage()
		attacking = true
		print("attacking=",attacking)
		
#func _on_attacking():
#	if attacking == true:
#		emit_signal("attack",1)
		

func _restore_full_health():
	if player_life < 20:
		player_life+=5

func _on_attackArea_area_entered(area):
	if area.is_in_group("inimigo_dia"):
		area.take_damage()
		attacking = true
		print("attacking=",attacking)




func _on_Event_Zone_body_entered(body):
	if body.is_in_group("Player"):
		puzzle_1 = true
		print(puzzle_1)
		


func _on_Event_Zone_body_exited(body):
	if body.is_in_group("Player"):
		puzzle_1 = false
		print(puzzle_1)

func _puzzle_1():
	if puzzle_1 == true:
		if Input.is_action_just_pressed("ITEM"):
			if $Light2D_outer.scala > 1.2:
				print("Passou o Puzzle 1")
