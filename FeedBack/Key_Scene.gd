extends Node2D


func _ready():
	$Key_Sprite/AnimationPlayer.play("Dorment")


func _waking():
	$Key_Sprite/AnimationPlayer.play("Waking")
	yield(get_tree().create_timer(4.5),"timeout")
	$Key_Sprite/AnimationPlayer.play("Idle")


func _on_PlayerMain_Puzzle1(on):
	_waking()


func _on_Key_Area_body_entered(body):
	print("corpo entrou chave 1")
	if body.is_in_group("Player"):
		print("Personagem pegou a chave 1")
		body._get_key()
		$Item_Collected.play()
		self.queue_free()


func _on_PlayerMain_Puzzle2(on):
	_waking()


func _on_PlayerMain_puzzle01(on):
	_waking()


func _on_Puzzle_01_unlockKey(i):
	_waking()


func _on_Puzzle_02_unlockKey2(i):
	_waking()


func _on_Sala_01_KeySala01(i):
	_waking()
	print("1 Recebeu sinal")


func _on_Sala_02_KeySala02(i):
	_waking()
	print("2 Recebeu sinal")


func _on_PlayerMain_Puzzle_3(i):
	_waking()
