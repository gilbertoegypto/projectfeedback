extends KinematicBody2D
var player = null;
var move = Vector2.ZERO
var speed = 100
export var move_speed = 80
var ativo = false
var visivel = false
export var life = 24
export var damaged_received = 1
var detected = false
var target = null
var facing_direction = Vector2(0,0)
var alive = true
var taking_damage = false
var attacking = false
signal defeated_boss(i)
var shield = true


func _ready():
	$AnimatedSprite/AnimationPlayer.play("Idle")
	$LifeBar.play("LifeBar_6")

func _physics_process(delta):
	
	if detected == true and alive == true and taking_damage == false and attacking == false:
		$AnimatedSprite/AnimationPlayer.play("Walking")
		move = target.global_position - self.global_position
		move = move.normalized()
		move_and_slide(move*move_speed)
		var collision = move_and_collide(move*delta)
		if collision:
			if collision.collider.is_in_group("Player"):
				attacking = true
		if move.x < 0:
			facing_direction = Vector2(-1, 0)
		elif move.x > 0:
			facing_direction = Vector2(1, 0)
		_changing_sprites()
	elif detected == false and alive == true and taking_damage == false and attacking == false:
		$AnimatedSprite/AnimationPlayer.play("Idle")
	elif detected == true and alive == true and taking_damage == false and attacking == true:
		$AnimatedSprite/AnimationPlayer.play("Attacking")
		yield(get_tree().create_timer(1.6),"timeout")
		attacking = false
	elif alive == false:
		pass
	
	if life>=24 and shield == true:
		$LifeBar.play("LifeBar_SH")
	if life>=24 and shield == false:
		$LifeBar.play("LifeBar_6")
	elif life<24 and life >=20:
		$LifeBar.play("LifeBar_5")
	elif life<20 and life >=16:
		$LifeBar.play("LifeBar_4")
	elif life<16 and life >=12:
		$LifeBar.play("LifeBar_3")
	elif life<12 and life >=8:
		$LifeBar.play("LifeBar_2")
	elif life<8 and life >=4:
		$LifeBar.play("LifeBar_1")
	elif life<=0:
		$LifeBar.play("LifeBar_0")


func _on_Area2D_body_entered(body):
	if body != self and ativo == true:
		player = body


func _on_Area2D_body_exited(body):
	player = null
	pass # Replace with function body.




func _changing_sprites():
	if move.x < 0:
		$AnimatedSprite.flip_h=false
	if move.x > 0:
		$AnimatedSprite.flip_h=true


func _on_DialogoText_lightsOn(on):
	ativo = true
	visivel = true
	self.set_collision_mask(8)
	self.set_collision_layer(8)


#func _on_Player_attack(atk):
#	take_damage()
	
func take_damage():
	if alive == true:
		if shield == false:
			life-=damaged_received
			taking_damage = true
			$AnimatedSprite/AnimationPlayer.play("Damage")
			yield(get_tree().create_timer(0.3),"timeout")
			taking_damage = false
			if life > 0:
				$AnimatedSprite/AnimationPlayer.play("Idle")
			elif life <=0:
				alive = false
				$AnimatedSprite/AnimationPlayer.play("Dying")
				yield(get_tree().create_timer(0.9),"timeout")
				$AnimatedSprite/AnimationPlayer.play("Dead")
				emit_signal("defeated_boss",1)





func _on_EnemyAttackArea_body_entered(body):
		if body.is_in_group("Player"):
			body.take_damage_boss()
			print("Boss Atacou")


func _on_EnemyAttackArea_area_entered(area):
		if area.is_in_group("Player"):
			area.take_damage_boss()

func _on_Detection_Area_body_entered(body):
	if body.is_in_group("Player"):
		detected = true
		target = body
	#else:
	#	detected = false
	#	target = Vector2(0,0)


func _on_Collect_Area_body_entered(body):
	if body.is_in_group("Player"):
		body._restore_full_health()
		$Item_Collected_Effect.play()
		$Item.queue_free()


func _on_Boss_Fight_Boss_Shild(i):
	shield = false
	print("Shield Down")
