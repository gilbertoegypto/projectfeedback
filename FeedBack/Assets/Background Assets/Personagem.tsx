<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="Personagem" tilewidth="64" tileheight="64" tilecount="16" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle down1.png"/>
 </tile>
 <tile id="1">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle down2.png"/>
 </tile>
 <tile id="2">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle down3.png"/>
 </tile>
 <tile id="3">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle down4.png"/>
 </tile>
 <tile id="4">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle left1.png"/>
 </tile>
 <tile id="5">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle left2.png"/>
 </tile>
 <tile id="6">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle left3.png"/>
 </tile>
 <tile id="7">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle left4.png"/>
 </tile>
 <tile id="8">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle right1.png"/>
 </tile>
 <tile id="9">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle right2.png"/>
 </tile>
 <tile id="10">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle right3.png"/>
 </tile>
 <tile id="11">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle right4.png"/>
 </tile>
 <tile id="12">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle up1.png"/>
 </tile>
 <tile id="13">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle up2.png"/>
 </tile>
 <tile id="14">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle up3.png"/>
 </tile>
 <tile id="15">
  <image width="64" height="64" source="Characters/Character with sword and shield/idle/idle up4.png"/>
 </tile>
</tileset>
