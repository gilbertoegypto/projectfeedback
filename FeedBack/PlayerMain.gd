extends KinematicBody2D

export var walk_speed = 210
export var movement = Vector2()
export var facingDir = Vector2()
export var interactDist = 70
export var flashLight = true
export var unlock =  false
export var hologram = 20
export var player_life = 30
signal interaction(command)
signal death(i)
export var flare = false
signal flare(location, facingdir)
signal activateLight(yorn)
signal hologramCollected(i)
var idle = true
var attack = false
signal attack(atk)
var attacking = false
var damage = false
var waking = false
var frus = true
var res_path
var cut
################################## sala 1 ################
signal sala1(i)
var sala1Key = 0
################################## sala 1 ################

################################## sala 2 ################
signal sala2(i)
var sala2Key = 0
################################## sala 2 ################

################################## puzzle 01 ################
export var push_speed = 125
signal puzzle01(on)
var Puzzle_01_Tip_on = false
################################## puzzle 01 ################

################################## puzzle 02 ################
signal puzzle02(i)
var Puzzle_02_Tip_on = false
################################## puzzle 02 ################


################################## puzzle 1 ################
export var key = 0

#var key2 = false #false
#Puzzle_1
var puzzle_1 = false
signal Puzzle1(on)
var Puzzle_1_Tip_on = false
################################## puzzle 1 ################

################################## puzzle 2 ################
var puzzle_2 = false
var turtle = false
var bear = false
var snake = false
var lion = false
var duck = false

var turtleLK = false
var bearLK = false
var snakeLK = false
var lionLK = false
var duckLK = false
signal Puzzle2(on)
var Puzzle_2_Tip_on = false
################################## puzzle 2 ################


################################## puzzle 3 ################

var puzzle3 = false
var box_puz3 = false
signal Puzzle_3(i)
var Puzzle_3_Tip_on = false
var azulTip = false
var cinzaTip = false
var verdeTip = false
var vermelhoTip = false
################################## puzzle 3 ################

func _ready():
	_Waking()
	_ui_sprites()
	#print(player_life)
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	$Camera2D/CanvasLayer/AnimatedSprite01.play("Full")
	$Camera2D/CanvasLayer/AnimatedSprite02.play("Full")
	$Camera2D/CanvasLayer/AnimatedSprite03.play("Full")
	$Camera2D/CanvasLayer/AnimatedSpriteKey1.play("NoKey")
	$Camera2D/CanvasLayer/AnimatedSpriteKey2.play("NoKey2")
	player_life = 30
	facingDir = Vector2(1,0)
	if facingDir.x < 0:
		$AnimatedSprite.set_flip_h( true )
		$Reflection.set_flip_h(true)
	if facingDir.x > 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	
func _Waking():
	#pass
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	$AnimatedSprite/AnimationPlayer.play("Idle_Left")
	$Reflection/AnimationPlayer.play("Idle_Left")
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")

func _physics_process(delta):
	_puzzle_1()
	_ui_sprites()
	_key_Collected()
	_puzzle3()
	################################## puzzle 01 ################
	#var movimento : = Vector2()
	################################## puzzle 01 ################
	var collision = move_and_collide(movement*delta)
	if collision:
		if collision.collider.is_in_group("inimigo_dia"):
			pass
		
	movement = Vector2()
	#Input
	if Input.is_action_pressed("DOWN") and  attack == false and damage == false and waking == false:
		movement.y+=1
		################################## puzzle 01 ################
		#movimento.y+=1
		################################## puzzle 01 ################
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		idle = false
	if Input.is_action_just_released("DOWN") and  attack == false and damage == false and waking == false:
		idle = true
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		
	if Input.is_action_pressed("UP") and  attack == false and damage == false and waking == false:
		movement.y+=-1
		################################## puzzle 01 ################
		#movimento.y+=-1
		################################## puzzle 01 ################
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		idle = false
		
	if Input.is_action_just_released("UP") and  attack == false and damage == false and waking == false:
		idle = true
		movement.y+=-1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		
	if Input.is_action_pressed("LEFT") and  attack == false and damage == false and waking == false:
		movement.x+=-1
		################################## puzzle 01 ################
		#movimento.x+=-1
		################################## puzzle 01 ################
		facingDir = Vector2(-1,0)
		idle = false
		
	if Input.is_action_just_released("LEFT") and  attack == false and damage == false and waking == false:
		idle = true
		facingDir = Vector2(-1,0)
			
	if Input.is_action_pressed("RIGHT") and  attack == false and damage == false and waking == false:
		movement.x+=1
		################################## puzzle 01 ################
		#movimento.x+=1
		################################## puzzle 01 ################
		facingDir = Vector2(1,0)
		idle = false
	if Input.is_action_just_released("RIGHT") and  attack == false and damage == false and waking == false:
		idle = true
		facingDir = Vector2(1,0)
			
	if Input.is_action_just_pressed("INTERACT") and  attack == false and damage == false and waking == false:
		attack = true
		#print(attack)
		idle = false
		#_on_attacking()
		yield(get_tree().create_timer(2.0),"timeout")
		idle = true
		attack = false
		#print(attack)
		
	if Input.is_action_just_pressed("ITEM")and frus == true and  attack == false and damage == false and waking == false and unlock == false:
		_Frustrated()
		
	#if Input.is_action_just_pressed("ITEM")and frus == false and  attack == false and damage == false and waking == false and unlock == true:
		#print("Apertou Item no if")
	#	if key >= 2:
			#print("Destrancou")
	#		res_path.unlock()
	#		key-= 2
	#		frus = true
	#		unlock = false
	#	elif key <2:
	#		_Error()
	
	if Input.is_action_just_pressed("ITEM")and frus == false and  attack == false and damage == false and waking == false and puzzle_2 == true:
		if lion == true:
			_Right()
			turtleLK = false
			bearLK = false
			snakeLK = false
			lionLK = true
			duckLK = false
		if turtle == true and lionLK == true:
			_Right()
			turtleLK = true
			bearLK = false
			snakeLK = false
			lionLK = true
			duckLK = false
		if duck==true and turtleLK == true and lionLK == true:
			_Right()
			turtleLK = true
			bearLK = false
			snakeLK = false
			lionLK = true
			duckLK = true
		if snake==true and duckLK==true and turtleLK == true and lionLK == true:
			_Right()
			turtleLK = true
			bearLK = false
			snakeLK = true
			lionLK = true
			duckLK = true
		if bear==true and snakeLK==true and duckLK==true and turtleLK == true and lionLK == true:
			_Right()
			turtleLK = true
			bearLK = true
			snakeLK = true
			lionLK = true
			duckLK = true
			
		if bearLK==true and snakeLK==true and duckLK==true and turtleLK == true and lionLK == true:
			#print("liberou puzzle 2")
			emit_signal("Puzzle2",1)
		
	if Input.is_action_just_pressed("ITEM") and Puzzle_01_Tip_on == true:
		#Puzzle_01_Tip_on = false
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Puzzle 01")
		yield(get_tree().create_timer(7.0),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	if Input.is_action_just_pressed("ITEM") and Puzzle_02_Tip_on == true:
		#Puzzle_02_Tip_on = false
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Puzzle 02")
		yield(get_tree().create_timer(7.0),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	if Input.is_action_just_pressed("ITEM") and Puzzle_1_Tip_on == true:
		#Puzzle_1_Tip_on = false
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Puzzle 1")
		yield(get_tree().create_timer(7.0),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	if Input.is_action_just_pressed("ITEM") and Puzzle_2_Tip_on == true:
		#Puzzle_2_Tip_on = false
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Puzzle 2")
		yield(get_tree().create_timer(7.0),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	if Input.is_action_just_pressed("ITEM") and Puzzle_3_Tip_on == true:
		#Puzzle_2_Tip_on = false
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Puzzle3")
		yield(get_tree().create_timer(7.0),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	if Input.is_action_just_pressed("ITEM") and azulTip == true:
		#Puzzle_2_Tip_on = false
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Azul")
		yield(get_tree().create_timer(7.0),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	if Input.is_action_just_pressed("ITEM") and verdeTip == true:
		#Puzzle_2_Tip_on = false
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Verde")
		yield(get_tree().create_timer(7.0),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	if Input.is_action_just_pressed("ITEM") and cinzaTip == true:
		#Puzzle_2_Tip_on = false
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Cinza")
		yield(get_tree().create_timer(7.0),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	if Input.is_action_just_pressed("ITEM") and vermelhoTip == true:
		#Puzzle_2_Tip_on = false
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Vermelho")
		yield(get_tree().create_timer(7.0),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
			
	_changing_sprites()
	movement=movement.normalized()
	#move the player
	move_and_slide(movement*walk_speed)
	################################## puzzle 01 ################
	#movimento=movimento.normalized()
	#move_and_slide(movimento*walk_speed)
	if get_slide_count() > 0:
		#print(movimento)
		check_box_collision(movement) 
	################################## puzzle 01 ################ 
	
func _changing_sprites():
	if facingDir.y!=0 and idle == false and  attack == false and damage == false and waking == false:
		$AnimatedSprite/AnimationPlayer.play("Walking_Left")
		if puzzle_2 == false:
			$Reflection/AnimationPlayer.play("Walking_Left")
			#$AnimatedSprite.set_flip_h( true )
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
			$Reflection.set_flip_h(false)
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
			$Reflection.set_flip_h(true)
		if puzzle_2 == true:
			if facingDir.x < 0:
				$Reflection.set_flip_h(false)
			if facingDir.x > 0:
				$Reflection.set_flip_h(true)
			if bear == true and turtle == false and lion == false and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Walking_Bear")
			if bear == false and turtle == true and lion == false and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Walking_Turtle")
			if bear == false and turtle == false and lion == true and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Walking_Lion")
			if bear == false and turtle == false and lion == false and snake == true and duck == false:
				$Reflection/AnimationPlayer.play("Walking_Snake")
			if bear == false and turtle == false and lion == false and snake == false and duck == true:
				$Reflection/AnimationPlayer.play("Walking_Duck")

	if facingDir.x!=0and idle == false and  attack == false and damage == false and waking == false:
		$AnimatedSprite/AnimationPlayer.play("Walking_Left")
		if puzzle_2 == false:
			$Reflection/AnimationPlayer.play("Walking_Left")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
			$Reflection.set_flip_h(false)
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
			$Reflection.set_flip_h(true)
		if puzzle_2 == true:
			if facingDir.x < 0:
				$Reflection.set_flip_h(false)
			if facingDir.x > 0:
				$Reflection.set_flip_h(true)
			if bear == true and turtle == false and lion == false and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Walking_Bear")
			if bear == false and turtle == true and lion == false and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Walking_Turtle")
			if bear == false and turtle == false and lion == true and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Walking_Lion")
			if bear == false and turtle == false and lion == false and snake == true and duck == false:
				$Reflection/AnimationPlayer.play("Walking_Snake")
			if bear == false and turtle == false and lion == false and snake == false and duck == true:
				$Reflection/AnimationPlayer.play("Walking_Duck")


			
	if idle == true and  attack == false and damage == false and waking == false:
		$AnimatedSprite/AnimationPlayer.play("Idle_Left")
		if puzzle_2 == false:
			$Reflection/AnimationPlayer.play("Idle_Left")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( true )
			$Reflection.set_flip_h(true)
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( false )
			$Reflection.set_flip_h(false)
		if puzzle_2 == true:
			if facingDir.x < 0:
				$Reflection.set_flip_h(false)
			if facingDir.x > 0:
				$Reflection.set_flip_h(true)
			if bear == true and turtle == false and lion == false and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Idle_Bear")
			if bear == false and turtle == true and lion == false and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Idle_Turtle")
			if bear == false and turtle == false and lion == true and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Idle_Lion")
			if bear == false and turtle == false and lion == false and snake == true and duck == false:
				$Reflection/AnimationPlayer.play("Idle_Snake")
			if bear == false and turtle == false and lion == false and snake == false and duck == true:
				$Reflection/AnimationPlayer.play("Idle_Duck")

			
	if idle == false and attack == true and damage == false:
		$AnimatedSprite/AnimationPlayer.play("Attacking")
		if puzzle_2 == false:
			$Reflection/AnimationPlayer.play("Attacking")
			#$AnimatedSprite.play("Attacking")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
			$Reflection.set_flip_h(false)
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
			$Reflection.set_flip_h(true)
		if puzzle_2 == true:
			if facingDir.x < 0:
				$Reflection.set_flip_h(false)
			if facingDir.x > 0:
				$Reflection.set_flip_h(true)
			if bear == true and turtle == false and lion == false and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Idle_Bear")
			if bear == false and turtle == true and lion == false and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Idle_Turtle")
			if bear == false and turtle == false and lion == true and snake == false and duck == false:
				$Reflection/AnimationPlayer.play("Idle_Lion")
			if bear == false and turtle == false and lion == false and snake == true and duck == false:
				$Reflection/AnimationPlayer.play("Idle_Snake")
			if bear == false and turtle == false and lion == false and snake == false and duck == true:
				$Reflection/AnimationPlayer.play("Idle_Duck")


		

func _ui_sprites():
	if player_life >= 30:
		$Camera2D/CanvasLayer/AnimatedSprite01.play("Full")
		$Camera2D/CanvasLayer/AnimatedSprite02.play("Full")
		$Camera2D/CanvasLayer/AnimatedSprite03.play("Full")
	if player_life >= 25 and player_life <30:
		$Camera2D/CanvasLayer/AnimatedSprite01.play("Full")
		$Camera2D/CanvasLayer/AnimatedSprite02.play("Full")
		$Camera2D/CanvasLayer/AnimatedSprite03.play("Half")
	if player_life >= 20 and player_life <25:
		$Camera2D/CanvasLayer/AnimatedSprite01.play("Full")
		$Camera2D/CanvasLayer/AnimatedSprite02.play("Full")
		$Camera2D/CanvasLayer/AnimatedSprite03.play("None")
	if player_life >= 15 and player_life <20:
		$Camera2D/CanvasLayer/AnimatedSprite01.play("Full")
		$Camera2D/CanvasLayer/AnimatedSprite02.play("Half")
		$Camera2D/CanvasLayer/AnimatedSprite03.play("None")
	if player_life >= 10 and player_life <15:
		$Camera2D/CanvasLayer/AnimatedSprite01.play("Full")
		$Camera2D/CanvasLayer/AnimatedSprite02.play("None")
		$Camera2D/CanvasLayer/AnimatedSprite03.play("None")
	if player_life >= 5 and player_life <10:
		$Camera2D/CanvasLayer/AnimatedSprite01.play("Half")
		$Camera2D/CanvasLayer/AnimatedSprite02.play("None")
		$Camera2D/CanvasLayer/AnimatedSprite03.play("None")
	if player_life < 1 and player_life <5:
		$Camera2D/CanvasLayer/AnimatedSprite01.play("None")
		$Camera2D/CanvasLayer/AnimatedSprite02.play("None")
		$Camera2D/CanvasLayer/AnimatedSprite03.play("None")
		
		
func take_damage():
	damage = true
	if player_life > 0 :
		_damage()
		#yield(get_tree().create_timer(3.0),"timeout")
	if player_life < 1 :
		#_damage()
		#pass
		#yield(get_tree().create_timer(3.0),"timeout")
		#emit_signal("death",1)
		self.visible = false
		yield(get_tree().create_timer(3.0),"timeout")
		get_tree().reload_current_scene()
		_restart()

	
func _restart():
	facingDir = Vector2(1,0)
	_Waking()
	
func _damage():
	_Pissed()
	player_life-=5
	print(player_life)
	$AnimatedSprite/AnimationPlayer.play("Damaged")
	$Reflection/AnimationPlayer.play("Damaged")
	if facingDir.x < 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	if facingDir.x > 0:
		$AnimatedSprite.set_flip_h( true )
		$Reflection.set_flip_h(true)
	yield(get_tree().create_timer(0.7),"timeout")
	$AnimatedSprite/AnimationPlayer.play("Idle_Left")
	$Reflection/AnimationPlayer.play("Idle_Left")
	if facingDir.x < 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	if facingDir.x > 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	damage = false
	
##################### boss #############################

func take_damage_boss():
	damage = true
	if player_life > 0 :
		_damage_boss()
		#yield(get_tree().create_timer(3.0),"timeout")
	if player_life < 1 :
		#_damage()
		#pass
		#yield(get_tree().create_timer(3.0),"timeout")
		#emit_signal("death",1)
		self.visible = false
		yield(get_tree().create_timer(3.0),"timeout")
		get_tree().reload_current_scene()
		_restart()


func _damage_boss():
	_Pissed()
	player_life-=10
	print(player_life)
	$AnimatedSprite/AnimationPlayer.play("Damaged")
	$Reflection/AnimationPlayer.play("Damaged")
	if facingDir.x < 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	if facingDir.x > 0:
		$AnimatedSprite.set_flip_h( true )
		$Reflection.set_flip_h(true)
	yield(get_tree().create_timer(0.7),"timeout")
	$AnimatedSprite/AnimationPlayer.play("Idle_Left")
	$Reflection/AnimationPlayer.play("Idle_Left")
	if facingDir.x < 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	if facingDir.x > 0:
		$AnimatedSprite.set_flip_h( false )
		$Reflection.set_flip_h(false)
	damage = false

#########################################################################


func _on_HologramItem1_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_HologramItem2_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_attackArea_body_entered(body):
	if body.is_in_group("inimigo_dia"):
		body.take_damage()
		attacking = true
		
		
#func _on_attacking():
#	if attacking == true:
#		emit_signal("attack",1)
		

func _on_attackArea_area_entered(area):
	if area.is_in_group("inimigo_dia"):
		area.take_damage()
		attacking = true
		#print("attacking=",attacking)

func _restore_full_health():
	if player_life < 30:
		player_life+= 5
		#print("Sua vida foi restaurada completamente!")
	else:
		pass
		#print("Você coletou item, porém sua vida já está cheia!")


func _Ballon_Popping():
	frus = false
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Poping")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Idle")
	yield(get_tree().create_timer(1.6),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Fading")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	frus = true
	
func _Frustrated():
	frus = false
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Poping")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Frustrated")
	yield(get_tree().create_timer(1.6),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Fading")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	frus = true
	
func _Error():
	frus = false
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Poping")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Error")
	yield(get_tree().create_timer(1.6),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Fading")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	frus = true
	
func _Pissed():
	frus = false
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Poping")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Pissed")
	yield(get_tree().create_timer(1.6),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Fading")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	frus = true

func _Right():
	frus = false
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Poping")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Right")
	yield(get_tree().create_timer(1.6),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Fading")
	yield(get_tree().create_timer(0.7),"timeout")
	$Interaction_Bubble/AnimatedSprite/AnimationPlayer.play("Dorment")
	frus = true
	

func _on_Event_Zone_body_entered(body):
	if body.is_in_group("Player"):
		puzzle_1 = true
		#print(puzzle_1)
		


func _on_Event_Zone_body_exited(body):
	if body.is_in_group("Player"):
		puzzle_1 = false
		#print(puzzle_1)

func _puzzle_1():
	if puzzle_1 == true:
		if Input.is_action_just_pressed("ITEM"):
			if $Light2D_outer.scala > 1.2:
				frus = false
				#print("Passou o Puzzle 1")
				_Right()
				emit_signal("Puzzle1",1)
				_Ballon_Popping()
				


func _on_Area_Puzzle2_1_body_entered(body):
	puzzle_2 = true
	duck = true
	_Ballon_Popping()


func _on_Area_Puzzle2_1_body_exited(body):
	puzzle_2 = false
	duck = false


func _on_Area_Puzzle2_2_body_entered(body):
	puzzle_2 = true
	lion = true
	_Ballon_Popping()

func _on_Area_Puzzle2_2_body_exited(body):
	puzzle_2 = false
	lion = false


func _on_Area_Puzzle2_3_body_entered(body):
	puzzle_2 = true
	snake = true
	_Ballon_Popping()

func _on_Area_Puzzle2_3_body_exited(body):
	puzzle_2 = false
	snake = false


func _on_Area_Puzzle2_4_body_entered(body):
	puzzle_2 = true
	bear = true
	_Ballon_Popping()

func _on_Area_Puzzle2_4_body_exited(body):
	puzzle_2 = false
	bear = false


func _on_Area_Puzzle2_5_body_entered(body):
	puzzle_2 = true
	turtle = true
	_Ballon_Popping()

func _on_Area_Puzzle2_5_body_exited(body):
	puzzle_2 = false
	turtle = false

func _get_key():
	key+=1
	$Item_Collected.play()
		

func _key_Collected():
	if key < 1:
		$Camera2D/CanvasLayer/AnimatedSpriteKey1.play("NoKey")
		$Camera2D/CanvasLayer/AnimatedSpriteKey2.play("NoKey2")
	if key == 1:
		$Camera2D/CanvasLayer/AnimatedSpriteKey1.play("NoKey")
		$Camera2D/CanvasLayer/AnimatedSpriteKey2.play("Key2")
	if key >=2:
		$Camera2D/CanvasLayer/AnimatedSpriteKey1.play("Key")
		$Camera2D/CanvasLayer/AnimatedSpriteKey2.play("Key2")
	
	
	
	
#func _unlockDoor():
#	_Ballon_Popping()
#	frus = false
#	if Input.is_action_just_pressed("ITEM"):
#		if key2 == true and key1 == true:
#			unlock = true



func _on_Unlock_Area_area_entered(area):
	if area.is_in_group("Door"):
		_Ballon_Popping()
		frus = false
		#print(frus)
		#print("Area Porta Chegou")
		unlock = true
		res_path = area.get_parent()
		if key >= 2:
			#print("Destrancou")
			res_path.unlock()
			key-= 2
			frus = true
			unlock = false
		elif key <2:
			_Error()


func _on_Unlock_Area_area_exited(area):
	unlock = false
	res_path = null
	frus = true
	#print("Saiu da área")
	
################################## puzzle 01 ################
func check_box_collision(moviment : Vector2) -> void:
	#if abs(moviment.x) + abs(moviment.y) >1:
		#return

	var box : = get_slide_collision(0).collider as Box
	if box:
		box.push(walk_speed*moviment)
		#print(push_speed * moviment * 10)
		#print(moviment)
		#print(push_speed)

################################## puzzle 01 ################


########################Tip_Func##############################
func Puzzle_01_Tip_area():
	Puzzle_01_Tip_on = true
	_Ballon_Popping()
	frus = false
	
func Puzzle_02_Tip_area():
	Puzzle_02_Tip_on = true
	_Ballon_Popping()
	frus = false
	
func Puzzle_1_Tip_area():
	Puzzle_1_Tip_on = true
	_Ballon_Popping()
	frus = false
	
func Puzzle_2_Tip_area():
	Puzzle_2_Tip_on = true
	_Ballon_Popping()
	frus = false
	
########################## Leaving Tip Area ######################
func Puzzle_01_Leaving_Tip_area():
	Puzzle_01_Tip_on = false
	frus = true

	
func Puzzle_02_Leaving_Tip_area():
	Puzzle_02_Tip_on = false
	frus = true
	
func Puzzle_1_Leaving_Tip_area():
	Puzzle_1_Tip_on = false
	frus = true
	
func Puzzle_2_Leaving_Tip_area():
	Puzzle_2_Tip_on = false
	frus = true



################################## puzzle 3 ################

func _on_Event_Zone_3_body_entered(body):
	if body.is_in_group("Player"):
		print("Na área p3")
		puzzle3 = true


func _on_Event_Zone_3_body_exited(body):
	if body.is_in_group("Player"):
		puzzle3 = false


func _on_Puzzle_02_boxex_in_place(i):
	box_puz3 = true
	print("Caixas na area p3")
	
func _puzzle3():
	if Input.is_action_just_pressed("ITEM"):
		if puzzle3 == true and box_puz3 == true:
			if $Light2D_outer.scala > 1.6:
				_Right()
				emit_signal("Puzzle_3",1)
				puzzle3 = false
				box_puz3 = false
				cinzaTip = false
				verdeTip = false
				vermelhoTip = false
				azulTip = false
		else:
			_Frustrated()
			
			
			
			
			
########################Tip_Func##############################
func Puzzle_3_Tip_area():
	Puzzle_3_Tip_on = true
	_Ballon_Popping()
	frus = false
	
func Puzzle_3_Azul_area():
	azulTip = true
	_Ballon_Popping()
	frus = false
	
func Puzzle_3_Verde_area():
	verdeTip = true
	_Ballon_Popping()
	frus = false
	
func Puzzle_3_Cinza_area():
	cinzaTip = true
	_Ballon_Popping()
	frus = false
	
func Puzzle_3_Vermelho_area():
	vermelhoTip = true
	_Ballon_Popping()
	frus = false
	
########################## Leaving Tip Area ######################
func Puzzle_3_Leaving_Tip_area():
	Puzzle_3_Tip_on = false
	frus = true

	
func Puzzle_3_Azul_Leaving():
	azulTip = false
	frus = true
	
func Puzzle_3_Verde_Leaving():
	verdeTip = false
	frus = true
	
func Puzzle_3_Vermelho_Leaving():
	verdeTip = false
	frus = true
	
func Puzzle_3_Cinza_Leaving():
	cinzaTip = false
	frus = true
################################## puzzle 3 ################


func _on_Boss_Event_body_entered(body):
	if body.is_in_group("Player"):
		waking = true
		$Background_track.stop()
		$Boss_track.play()
		$AnimatedSprite/AnimationPlayer.play("Cut_Scene_Boss")
		yield(get_tree().create_timer(3.5),"timeout")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Boss_1")
		yield(get_tree().create_timer(6.5),"timeout")
		$AnimatedSprite/AnimationPlayer.play("Idle_Left")
		$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Idle")
		waking = false
		
func ending():
	$Dialogue_Box/AnimatedSprite/AnimationPlayer.play("Ending")
	$Light2D_outer.visible = false
	$Light2d_inner.visible = false
	waking = true
	yield(get_tree().create_timer(9.5),"timeout")
	get_tree().change_scene("res://Ending_Card.tscn")
	

func _on_Boss_defeated_boss(i):
	$Background_track.stop()
	$Boss_track.stop()
	$Tranquiity_track.play()
