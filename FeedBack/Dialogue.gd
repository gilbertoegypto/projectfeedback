extends Label
signal lightsOn(on)


func _on_Event_Area_Corridor_body_entered(body):
	print("Area entered")
	text = ("É melhor eu não entrar aí!")
	yield(get_tree().create_timer(3.0),"timeout")
	text = ("")


func _on_Event_Area_Begin_body_entered(body):
	print("Area entered")
	text = ("O painel de controle central se encontra na cafetéria")
	yield(get_tree().create_timer(3.5),"timeout")
	text = ("Reconecte a energia para poder escapar!")
	yield(get_tree().create_timer(3.5),"timeout")
	text = ("")


func _on_Player_interaction(command):
	if command == "noKey":
		text = ("VOCÊ PRECISA RECUPERAR AS 4 CHAVES DE SEGURANÇA!")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("AS CHAVES SE ENCONTRAM EM COMPUTADORES")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("AINDA LIGADOS ESPALHADOS NESTE ANDAR")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("")
	if command == "allKey":
		text = ("AVISO: AO ATIVAR A CENTRAL DE ENERGIA DO ANDAR TODAS AS LUZES SERÃO ACESAS!")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("")
		yield(get_tree().create_timer(1.5),"timeout")
		text = ("3")
		yield(get_tree().create_timer(1.5),"timeout")
		text = ("2")
		yield(get_tree().create_timer(1.5),"timeout")
		text = ("1")
		yield(get_tree().create_timer(1.5),"timeout")
		text = ("")
		emit_signal("lightsOn",1)


func _on_Player_keyCollected(key):
	if key == "A":
		text = ("Chave A foi coletada")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("")
	if key == "B":
		text = ("Chave B foi coletada")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("")
	if key == "C":
		text = ("Chave C foi coletada")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("")
	if key == "D":
		text = ("Chave D foi coletada")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("")


func _on_Player_unlocked(lock):
	if lock == "no":
		text = ("VOCÊ PRECISA REATIVAR A ENERGIA NO ANDAR!")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("")
	elif lock =="yes":
		text = ("VOCÊ ACIONOU A PORTA PARA SAIR!")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("")
		yield(get_tree().create_timer(3.0),"timeout")
		get_node("Level").end_level()


func _on_Event_Area_Maintanance_body_entered(body):
	text = ("A conexão da intranet foi cortada junto com a energia!")
	yield(get_tree().create_timer(3.0),"timeout")
	text = ("")


func _on_Event_AreaServerRoom2_body_entered(body):
	text = ("Tem uma anotação aqui:")
	yield(get_tree().create_timer(3.0),"timeout")
	text = (" 'As amostras coletadas no planeta Kepler 22b parecem instáveis' ")
	yield(get_tree().create_timer(3.0),"timeout")
	text = (" 'Recebemos hoje uma ordem da diretoria nos informou que devemos continuar'")
	yield(get_tree().create_timer(3.0),"timeout")
	text = (" 'com os testes utilizando eletrochoque!'")
	yield(get_tree().create_timer(3.0),"timeout")
	text = ("")


func _on_Event_Area_Corridor_Death_body_entered(body):
	text = ("SE FUDEU!")
	yield(get_tree().create_timer(3.0),"timeout")


func _on_Player_flareDialogue(yorn):
	if yorn == true:
		text = ("Você coletou um flare de emergência")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("Clique no botão 'R' para solta-lo e ele irá chamar atenção do inimígo")
		yield(get_tree().create_timer(4.0),"timeout")
		text = ("")
	if yorn == false:
		text = ("Você só pode carregar um flare de emergência por vez")
		yield(get_tree().create_timer(4.0),"timeout")
		text = ("")


func _on_Player_hologramCollected(i):
		text = ("Você coletou projetor de holograma")
		yield(get_tree().create_timer(3.0),"timeout")
		text = ("Clique no botão 'E' para ativa-lo e ele irá chamar atenção do inimígo")
		yield(get_tree().create_timer(4.0),"timeout")
		text = ("")


func _on_Player_death(i):
	text = ("SE FUDEU!")
	yield(get_tree().create_timer(3.0),"timeout")
