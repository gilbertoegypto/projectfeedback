extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal key(i)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_PrimeiraSalaEvent_body_exited(body):
	if body.is_in_group("inimigo_dia"):
		emit_signal("key",1)
		self.queue_free()
