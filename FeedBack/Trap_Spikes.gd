extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite/AnimationPlayer.play("running")





func _on_hurt_box_body_entered(body):
	if body.is_in_group("Player"):
		body.take_damage()
