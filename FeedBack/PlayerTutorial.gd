extends KinematicBody2D

export var walk_speed = 250
export var movement = Vector2()
export var facingDir = Vector2()
export var interactDist = 70
export var flashLight = true
export var KeyA = false
export var KeyB = false
export var KeyC = false
export var KeyD = false
export var unlock = false
export var hologram = 20
signal interaction(command)
signal keyCollected(key)
signal unlocked(lock)
signal hologram(location)
signal death(i)
export var flare = false
signal flare(location, facingdir)
signal activateLight(yorn)
signal hologramCollected(i)
var idle = true
var attack = false
signal attack(atk)
var attacking = false

func _ready():
	$Light2D_outer.enabled = not $Light2D_outer.enabled
	$Light2d_inner.enabled = not $Light2d_inner.enabled
	facingDir = Vector2(1,0)
	_Waking()

func _Waking():
	$AnimatedSprite.play("Waking")
	yield(get_tree().create_timer(2.66),"timeout")
	$AnimatedSprite.play("Idle_Left")

func _physics_process(delta):
	var collision = move_and_collide(movement*delta)
	if collision:
		if collision.collider.is_in_group("inimigo"):
			emit_signal("death",1)
			self.visible = false
			yield(get_tree().create_timer(3.0),"timeout")
			get_tree().reload_current_scene()
		if collision.collider.is_in_group("inimigo_dia"):
			emit_signal("death",1)
			self.visible = false
			yield(get_tree().create_timer(3.0),"timeout")
			get_tree().reload_current_scene()
		
	movement = Vector2()
	#Input
	if Input.is_action_pressed("DOWN") and  attack == false:
		movement.y+=1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		idle = false
	if Input.is_action_just_released("DOWN") and  attack == false:
		idle = true
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		
	if Input.is_action_pressed("UP") and  attack == false:
		movement.y+=-1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		idle = false
		
	if Input.is_action_just_released("UP") and  attack == false:
		idle = true
		movement.y+=-1
		if facingDir == Vector2(-1,0):
			facingDir = Vector2(-1,0)
		elif facingDir == Vector2(1,0):
			facingDir = Vector2(1,0)
		
	if Input.is_action_pressed("LEFT") and  attack == false:
		movement.x+=-1
		facingDir = Vector2(-1,0)
		idle = false
		
	if Input.is_action_just_released("LEFT") and  attack == false:
		idle = true
		facingDir = Vector2(-1,0)
			
	if Input.is_action_pressed("RIGHT") and  attack == false:
		movement.x+=1
		facingDir = Vector2(1,0)
		idle = false
	if Input.is_action_just_released("RIGHT") and  attack == false:
		idle = true
		facingDir = Vector2(1,0)
			
	if Input.is_action_just_pressed("INTERACT") and  attack == false:
		attack = true
		print(attack)
		idle = false
		_on_attacking()
		yield(get_tree().create_timer(2.0),"timeout")
		idle = true
		attack = false
		print(attack)
		
		
		
	
	_changing_sprites()
	movement=movement.normalized()
	#move the player
	move_and_slide(movement*walk_speed)
	
	
func _changing_sprites():
	if facingDir.y!=0 and idle == false and  attack == false:
		$AnimatedSprite.play("Walking_Left")
		#$AnimatedSprite.set_flip_h( true )

	if facingDir.x!=0and idle == false and  attack == false:
		$AnimatedSprite.play("Walking_Left")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
			
	if idle == true and  attack == false:
		$AnimatedSprite.play("Idle_Left")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( true )
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( false )
			
	if idle == false and attack == true:
		$AnimatedSprite.play("Attacking")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
		
		
func _flash_light():
	flashLight = false
	yield(get_tree().create_timer(10.0),"timeout")
	flashLight = true


func _on_Event_Area_Cafeteria_body_entered(body):
	if KeyB == true && KeyA == true && KeyC == true && KeyD == true: 
		emit_signal("interaction", "allKey")
	else:
		emit_signal("interaction","noKey")



func _on_Event_Area_Lab_body_entered(body):
	if KeyB == false:
		KeyB = true
		emit_signal("keyCollected", "B")
	elif KeyB == true:
		pass


func _on_Event_Area_Maintanance_body_entered(body):
	if KeyA == false:
		KeyA = true
		emit_signal("keyCollected", "A")
	elif KeyA == true:
		pass


func _on_Event_Area_Exit_body_entered(body):
	if unlock == false:
		emit_signal("unlocked","no")
	if unlock == true:
		emit_signal("unlocked","yes")


func _on_DialogoText_lightsOn(on):
	unlock = true


func _on_Event_Area_ComputerSecurity_body_entered(body):
	if KeyC == false:
		KeyC = true
		emit_signal("keyCollected", "C")
	elif KeyC == true:
		pass


func _on_Event_Area_1ServerRoom2_body_entered(body):
	if KeyD == false:
		KeyD = true
		emit_signal("keyCollected", "D")
	elif KeyD == true:
		pass


func _on_Event_Area_Corridor_Death_body_entered(body):
	self.visible = false
	yield(get_tree().create_timer(3.0),"timeout")
	get_tree().reload_current_scene()



func _on_HologramItem1_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_HologramItem2_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_Area2D_body_entered(body):
	$Light2D_outer.enabled =  not $Light2D_outer.enabled
	$Light2d_inner.enabled =  not $Light2d_inner.enabled
	emit_signal("activateLight", true)


func _on_attackArea_body_entered(body):
	if body.is_in_group("inimigo_dia"):
		attacking = true
func _on_attacking():
	if attacking == true:
		emit_signal("attack",1)


func _on_PlayerTutorial_activateLight(yorn):
	#$Light2D_outer.enabled = true $Light2D_outer.enabled
	#$Light2d_inner.enabled = true $Light2d_inner.enabled
	pass
