extends Light2D
onready var noise = OpenSimplexNoise.new()
var value = 0.0
const MAX_VALUE = 10000
var scala = 2

var neuro_fb = 0


func _ready():
	_on_Timer_timeout()
	visible=true
	randomize()
	value = randi() % MAX_VALUE
	noise.period = 8
	
	
func _physics_process(delta):
	self.texture_scale = scala
	value+=0.5
	if(value>MAX_VALUE):
		value = 0.0
	var alpha  = ((noise.get_noise_1d(value)+1)/4.0)+0.5
	self.color = Color(color.r,color.g,color.b,alpha)
	
	if Input.is_action_pressed("aumentar"):
		if scala < 2:
			scala+=0.1
	if Input.is_action_pressed("diminuir"):
		if scala > 0:
			scala-=0.1
	
	
	#if neuro_fb == 0:
	#	scala = 1
		#scala = lerp(scala, 2, delta)
	#elif neuro_fb == 1:
	#	scala = 0.8
		#scala = lerp(scala, 1.6, delta)
	#elif neuro_fb == 2:
	#	scala = 0.6
		#scala = lerp(scala, 1.2, delta)
	#elif neuro_fb == 3:
	#	scala = 0.4
		#scala = lerp(scala, 0.8, delta)
	#elif neuro_fb == 4:
	#	scala = 0.2
		#scala = lerp(scala, 0.4, delta)
	#elif neuro_fb == 5:
	#	scala = 0.0
		#scala = lerp(scala, 0.0, delta)
		

func _on_DialogoText_lightsOn(on):
	pass


func _on_Player_activateLight(yorn):
	visible=true


func _on_Timer_timeout():
	#neuro_fb = floor(rand_range(0,6))
	pass
