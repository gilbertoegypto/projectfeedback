extends Area2D

signal position(local)
var pos = Vector2(0,0)
var destination = Vector2(0,0)

func _ready():
	pos = self.global_position
	emit_signal("position", pos)
	



func _on_Door_2_body_entered(body):

	if body.is_in_group("Player"):
		body.global_position = destination


func _on_Door_2_area_entered(area):

	if area.is_in_group("Player"):
		area.global_position = destination




func _on_Door_2_position(local):
	destination = local
