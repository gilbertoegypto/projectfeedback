extends KinematicBody2D

var motion = Vector2()
var dir = 0
var Xdir = 0 
var Ydir = 0
var speed = 30
var rangE = 0
func _ready():
	_on_Timer_timeout()
	#pass 

func _physics_process(delta):
	
	if dir == 0:
		#motion = Vector2(-Xdir,-Ydir)
		motion.y = lerp(motion.y, -Ydir, delta)
		motion.x = lerp(motion.x, -Xdir, delta)
	elif dir == 1:
		#motion = Vector2(Xdir,Ydir)*rangE
		motion.y = lerp(motion.y, Ydir, delta)
		motion.x = lerp(motion.x, Xdir, delta)
	elif dir ==2:
		#motion = Vector2(-Xdir,Ydir)*rangE
		motion.y = lerp(motion.y, Ydir, delta)
		motion.x = lerp(motion.x, -Xdir, delta)
	elif dir == 3:
		#motion = Vector2(-Xdir,-Ydir)*rangE
		motion.y = lerp(motion.y, -Ydir, delta)
		motion.x = lerp(motion.x, -Xdir, delta)
	elif dir == 4:
		#motion = Vector2(Xdir,-Ydir)*rangE
		motion.y = lerp(motion.y, -Ydir, delta)
		motion.x = lerp(motion.x, Xdir, delta)
		
	if Input.is_action_pressed("MouseClick"):
		motion = self.get_global_mouse_position() - position
	
	move_and_slide(motion, Vector2(0,0))
	var normalized_motion = motion.normalized()
	move_and_slide(normalized_motion*speed)
	#motion = self.get_global_mouse_position() - position
#func _randomization(time):
#	dir = floor(rand_range(0,5))
#	yield(get_tree().create_timer(time),"timeout")
	

func _on_Timer_timeout():
	dir = floor(rand_range(0,5))
	Xdir = floor(rand_range(2,6))
	Ydir = floor(rand_range(2,6))

	yield(get_tree().create_timer(3.0),"timeout")
	#$Timer.start()


