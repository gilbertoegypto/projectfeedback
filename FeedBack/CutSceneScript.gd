extends KinematicBody2D

export var walk_speed = 250
export var movement = Vector2()
var start = false

func _ready():
	start = true
	pass # Replace with function body.

func _physics_process(delta):
	movement = Vector2()
	if start == true:
		movement.x+=0.01
	
	movement=movement.normalized()
	
	move_and_slide(movement*walk_speed)
