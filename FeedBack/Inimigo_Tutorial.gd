extends KinematicBody2D
var player = null;
var move = Vector2.ZERO
var speed = 100
export var move_speed = 80
var ativo = false
var visivel = false
export var life = 6
export var damaged_received = 1
var detected = false
var target = null
var facing_direction = Vector2(0,0)
var alive = true
var taking_damage = false
var attacking = false
var wake = false


func _ready(): #1.7
	$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Dorment")
	$LifeBar.play("LifeBar_0")

func _physics_process(delta):
	
	if detected == true and alive == true and taking_damage == false and attacking == false and wake == true:
		$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Enemy_Walking")
		move = target.global_position - self.global_position
		move = move.normalized()
		move_and_slide(move*move_speed)
		var collision = move_and_collide(move*delta)
		if collision:
			if collision.collider.is_in_group("Player"):
				attacking = true
		if move.x < 0:
			facing_direction = Vector2(-1, 0)
		elif move.x > 0:
			facing_direction = Vector2(1, 0)
		_changing_sprites()
	elif detected == false and alive == true and taking_damage == false and attacking == false and wake == true:
		$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Enemy_Idle")
	elif detected == true and alive == true and taking_damage == false and attacking == true and wake == true:
		$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Enemy_Attacking")
		yield(get_tree().create_timer(2.0),"timeout")
		attacking = false
	elif alive == false:
		pass
	if wake == true:
		if life>=6:
			$LifeBar.play("LifeBar_6")
		elif life<6 and life >=5:
			$LifeBar.play("LifeBar_5")
		elif life<5 and life >=4:
			$LifeBar.play("LifeBar_4")
		elif life<4 and life >=3:
			$LifeBar.play("LifeBar_3")
		elif life<3 and life >=2:
			$LifeBar.play("LifeBar_2")
		elif life<2 and life >=1:
			$LifeBar.play("LifeBar_1")
		elif life<=0:
			$LifeBar.play("LifeBar_0")


func _on_Area2D_body_entered(body):
	if body != self and ativo == true:
		player = body


func _on_Area2D_body_exited(body):
	player = null
	pass # Replace with function body.




func _changing_sprites():
	if move.x < 0:
		$Sprite/EnemyAnimation/EnemyAnimationSprite.flip_h=true
	if move.x > 0:
		$Sprite/EnemyAnimation/EnemyAnimationSprite.flip_h=false


#func _on_DialogoText_lightsOn(on):
#	ativo = true
#	visivel = true
#	self.set_collision_mask(8)
#	self.set_collision_layer(8)


#func _on_Player_attack(atk):
#	take_damage()
	
func take_damage():
	if  wake == true and alive == true:
		life-=damaged_received
		taking_damage = true
		#print(life)
		$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Enemy_Damaged")
		yield(get_tree().create_timer(2.0),"timeout")
		taking_damage = false
		if life > 0:
			$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Enemy_Idle")
		elif life <=0:
			alive = false
			$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Enemy_Dying")
			yield(get_tree().create_timer(2.8),"timeout")
			$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Dead")




func _on_EnemyAttackArea_body_entered(body):
		if body.is_in_group("Player"):
			body.take_damage()


func _on_EnemyAttackArea_area_entered(area):
		if area.is_in_group("Player"):
			area.take_damage()

func _on_Detection_Area_body_entered(body):
	if body.is_in_group("Player"):
		detected = true
		target = body
	#else:
	#	detected = false
	#	target = Vector2(0,0)


func wakeUp():
	$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Enemy_Rising")
	yield(get_tree().create_timer(1.7),"timeout")
	$Sprite/EnemyAnimation/EnemyAnimationSprite/AnimationPlayer.play("Enemy_Idle")
	$LifeBar.play("LifeBar_6")
	wake = true

func _on_Event_Waking_Enemy_area_entered(area):
	if area.is_in_group("Player"):
			wakeUp()


func _on_Event_Waking_Enemy_body_entered(body):
	if body.is_in_group("Player"):
			wakeUp()


func _on_Collect_Area_body_entered(body):
	if body.is_in_group("Player"):
		body._restore_full_health()
		$Item_Collected_Effect.play()
		$Item.queue_free()
		


func _on_Puzzle_01_unlockKey(i):
	wakeUp()


func _on_PlayerMain_Puzzle2(on):
	wakeUp()


func _on_PlayerMain_Puzzle1(on):
	wakeUp()


func _on_PlayerMain_puzzle01(on):
	wakeUp()


func _on_PlayerMain_Puzzle_3(i):
	wakeUp()


func _on_Puzzle_4_unlockKey2(i):
	wakeUp()
