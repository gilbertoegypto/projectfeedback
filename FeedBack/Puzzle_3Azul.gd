extends Area2D







func _on_Puzzle_3Azul_body_entered(body):
	if body.is_in_group("Player"):
		body.Puzzle_3_Azul_area()


func _on_Puzzle_3Azul_body_exited(body):
	if body.is_in_group("Player"):
		body.Puzzle_3_Azul_Leaving()
