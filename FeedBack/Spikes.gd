extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Spikes_animation/AnimationPlayer.play("Idle")

	

func _on_Hurt_box_body_entered(body):
	if body.is_in_group("Player"):
		body.take_damage()


func _on_Hurt_box_area_entered(area):
	if area.is_in_group("Player"):
		area.take_damage()
