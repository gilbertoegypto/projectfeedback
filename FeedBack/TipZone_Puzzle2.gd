extends Area2D

func _on_TipZone_body_entered(body):
	if body.is_in_group("Player"):
		body.Puzzle_2_Tip_area()


func _on_TipZone_body_exited(body):
	if body.is_in_group("Player"):
		body.Puzzle_2_Leaving_Tip_area()
