extends Area2D



func _on_Puzzle_3Cinza_body_entered(body):
	if body.is_in_group("Player"):
		body.Puzzle_3_Cinza_area()


func _on_Puzzle_3Cinza_body_exited(body):
	if body.is_in_group("Player"):
		body.Puzzle_3_Cinza_Leaving()
