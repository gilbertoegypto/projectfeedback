extends Area2D


func _on_Puzzle_3_tip_body_entered(body):
	if body.is_in_group("Player"):
		body.Puzzle_3_Tip_area()


func _on_Puzzle_3_tip_body_exited(body):
	if body.is_in_group("Player"):
		body.Puzzle_3_Leaving_Tip_area()
