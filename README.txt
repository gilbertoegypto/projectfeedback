Esse é um projeto para a cadeira de Experimentação de Protótipo da faculdade de Ciência da Computação na UNIFOR.
Ele consiste da criação de um jogo eletrônico utilizando a Engine GODOT.
Projeto com objetivo de criação de jogo eletrônico com interação de controle utilizado no tratamento via Neurofeedback. Projeto foi um de quatro escolhidos entre vinte e seis projetos para ser apresentado no dia T da UNIFOR em 2021.
Eu fui responsável pela criação do jogo enquanto o outro membro da equipe se responsabilizou pela criação do controle.
Arquivo deve ser aberto na Engine Godot e para executar deve-se clicar no F6. Os botões para jogar são os direcionais WASD e E para atacar, R para interagir com objetos e os botões “+” e “-“ do numpad servindo para alterar a luminosidade do jogo, simulando a interação com o neurofeedback.
Vídeo apresentação do projeto: 
Parte 1 https://www.youtube.com/watch?v=f3k0EjGIDxI
Parte 2 https://www.youtube.com/watch?v=mzsRNCyCUig

Por Gilberto Egypto
